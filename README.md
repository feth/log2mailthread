# log2mailthread

Override standard lib SMTPHandler with e-mail threading ability.

1) generate MessageID, put it in the relevant e-mail header
2) check if there is a last_messageID, and set a "References" header if so.
3) set last_messageID